sudo apt-get install mysql-server
(optional:
 sudo apt-get install phpmyadmin

#allow remote access:
/etc/mysql/my.cnf:
  bind-address            = 0.0.0.0

#create sql user & db: hustoj
mysql -u root -p
 set names utf8;
 CREATE USER 'hustoj'@'%' IDENTIFIED BY 'hustoj';
 create database hustoj;
 GRANT ALL PRIVILEGES ON hustoj.* TO 'hustoj'@'%';
 FLUSH PRIVILEGES;

#create data tables
mysql -u hustoj -p
 set names utf8;
 use hustoj;
 source db.sql
