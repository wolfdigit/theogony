#!/bin/sh
FILEHOST=127.0.0.1
WEBPATH=/var/www/Theogony
WEBUSER=www-data
WEBUSERHOME=/var/www

if [ "x$FILEHOST" = "x0.0.0.0" ]; then
	echo "plz edit Hermes/install.sh to set FILEHOST properly"
	return 1
fi

sudo apt-get install apache2 php5 php5-mysql php5-gd

sudo mkdir $WEBPATH
sudo cp -r web/* $WEBPATH
sudo mkdir $WEBPATH/cache $WEBPATH/upload
sudo chown -R $WEBUSER $WEBPATH/cache $WEBPATH/upload

sudo mkdir $WEBUSERHOME/.ssh
sudo chown $WEBUSER $WEBUSERHOME/.ssh
echo "        <Directory $WEBUSERHOME/.ssh>" > hide_dot_ssh.conf
echo "                        Deny from all" >> hide_dot_ssh.conf
echo "        </Directory>" >> hide_dot_ssh.conf
sudo cp hide_dot_ssh.conf /etc/apache2/conf.d/
sudo /etc/init.d/apache2 restart
sudo -u $WEBUSER ssh-keygen
sudo cat $WEBUSERHOME/.ssh/id_rsa.pub
echo "copy this key to /home/judge/.ssh/authorized_keys @ $FILEHOST"
echo "(cmd: ""sudo -u judge vim /home/judge/.ssh/authorized_keys"" on $FILEHOST)"
read DUMMY
echo "this time it may ask to confirm host key, but no passwd:"
read DUMMY
sudo -u $WEBUSER ssh judge@$FILEHOST echo "SUCCESS: it should not ask for passwd"
#read -n 1 -p "exec ""sudo -u www-data ssh judge@FILEHOST"", it may ask to confirm host key, but no passwd"

echo " configure: ""sudo vim $WEBPATH/include/db_info.inc.php"" "
read DUMMY
sudo vim $WEBPATH/include/db_info.inc.php
