<?php 
$cache_time=1;
require_once("oj-header.php");
require_once("include/db_info.inc.php");

?>
<center>
<form class="pure-form pure-form-aligned" action="register.php" method="post">
    <fieldset>
		<legend>注冊資料</legend>
        <div class="pure-control-group">
            <label for="name">帳號（學號）:</label>
			<input name="user_id" id="user_id" type="text" placeholder="Username" required>
        </div>

		 <div class="pure-control-group">
            <label for="nick">暱稱:</label>
            <input name="nick" id="nick" type="text" placeholder="Nickname">
        </div>
		
        <div class="pure-control-group">
            <label for="password">密碼:</label>
            <input name="password" id="password" type="password" placeholder="Password" required>
        </div>

		<div class="pure-control-group">
            <label for="rptpassword">重複密碼:</label>
            <input name="rptpassword" id="rptpassword" type="password" placeholder="Password" required>
        </div>
        <div class="pure-control-group">
            <label for="email">電子郵件:</label>
            <input id="email" type="email" placeholder="Email Address">
        </div>
		
		<?php if($OJ_VCODE){?>
        <div class="pure-control-group">
            <label for="vcode">驗證碼:</label>
            <input name="vcode" id="vcode" type="text" size=4><img src=vcode.php required>
        </div>
		<div class="pure-control-group">
		</div>
		<?php }?>
		
        <div class="pure-controls">
			<input value="Submit" class="pure-button button-primary" namee="submit" type="submit">
			<input value="Reset" class="pure-button button-primary" name="reset" type="reset">
        </div>
    </fieldset>
</form>
<?php /*
<form class="pure-form pure-form-stacked">
    <fieldset>
		<legend><h2>或是使用以下的網站登入</h2></legend>
		<a href="oauth/index.php?provider=google&action=auth" class="pure-button button-primary" >Google 登入</a>&nbsp;
		<?php if(isset($_GET["lssc_overwrite"])) echo '<a href="oauth.php/index.php?provider=lssc&action=auth" class="pure-button button-primary" >LSSC 登入</a>'; ?>
    </fieldset>
</form>
*/ ?>
</center>
<?php require_once("oj-footer.php");?>
