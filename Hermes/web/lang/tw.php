<?php

        $MSG_FAQ="常見問答";
        $MSG_BBS="討論版";
        $MSG_HOME="首頁";
        $MSG_PROBLEMS="問題";
        $MSG_STATUS="狀態";
        $MSG_RANKLIST="排名";
        $MSG_CONTEST="競賽";
        $MSG_CONTESTRANKLIST="競賽總排名";
        $MSG_LOGOUT="登出";
        $MSG_LOGIN="登入";
        $MSG_REGISTER="註冊";
        $MSG_ADMIN="管理";
        $MSG_STANDING="名次";
        $MSG_STATISTICS="統計";
        $MSG_USERINFO="修改帳號";
        $MSG_MAIL="短消息";

        $MSG_Pending="等待中";
        $MSG_Pending_Rejudging="等待重判";
        $MSG_Compiling="編譯中";
        $MSG_Running_Judging="執行並評判";
        $MSG_Accepted="正確";
        $MSG_Presentation_Error="格式錯誤";
        $MSG_Wrong_Answer="答案錯誤";
        $MSG_Time_Limit_Exceed="時間超限";
        $MSG_Memory_Limit_Exceed="記憶體超限";
        $MSG_Output_Limit_Exceed="輸出超限";
        $MSG_Runtime_Error="執行錯誤";
        $MSG_Compile_Error="編譯錯誤";
        $MSG_Compile_OK="編譯成功";
        $MSG_XD="學弟乖~快去練比賽~";

        $MSG_PD="等待中";
        $MSG_PR="等待重判";
        $MSG_CI="編譯中";
        $MSG_RJ="執行並評判";
        $MSG_AC="正確";
        $MSG_PE="格式錯誤";
        $MSG_WA="答案錯誤";
        $MSG_TLE="時間超限";
        $MSG_MLE="記憶體超限";
        $MSG_OLE="輸出超限";
        $MSG_RE="執行錯誤";
        $MSG_CE="編譯錯誤";
        $MSG_CO="編譯成功";

        $MSG_RUNID="執行編號";
        $MSG_USER="用戶";
        $MSG_PROBLEM="問題";
        $MSG_RESULT="結果";
        $MSG_MEMORY="記憶體";
        $MSG_TIME="耗時";
        $MSG_LANG="語言";
        $MSG_CODE_LENGTH="程式碼長度";
        $MSG_SUBMIT_TIME="提交時間";

        $MSG_SEARCH="搜尋";
        $MSG_PROBLEM_ID="題目編號";
        $MSG_TITLE="標題";
        $MSG_SOURCE="來源";
        $MSG_SUBMIT="提交";

        //ranklist.php
        $MSG_Number="名次";
        $MSG_NICK="暱稱";
        $MSG_SOVLED="答題";
        $MSG_RATIO="比率";

        //registerpage.php
        $MSG_USER_ID="帳號（學號）";
        $MSG_PASSWORD="密碼";
        $MSG_REPEAT_PASSWORD="重複密碼";
        $MSG_SCHOOL="學校";
        $MSG_EMAIL="電子郵件";
        $MSG_REG_INFO="注冊資料";
        $MSG_VCODE="驗證碼";

                //problem.php
        $MSG_NO_SUCH_PROBLEM="題目不可用!";
        $MSG_Description="題目描述"  ;
        $MSG_Input="輸入"  ;
        $MSG_Output= "輸出" ;
        $MSG_Sample_Input= "範例輸入" ;
        $MSG_Sample_Output= "範例輸出" ;
        $MSG_HINT= "提示" ;
        $MSG_Source= "來源" ;
        $MSG_Time_Limit="時間限制";
        $MSG_Memory_Limit="記憶體限制";

        //admin menu
        $MSG_SEEOJ="查看前台";
        $MSG_ADD="添加";
        $MSG_LIST="列表";
        $MSG_NEWS="新聞";
        $MSG_TEAMGENERATOR="比賽隊伍帳號產生器";
        $MSG_SETMESSAGE="設置跑馬燈";
        $MSG_SETPASSWORD="修改密碼";
        $MSG_REJUDGE="重判題目";
        $MSG_PRIVILEGE="權限";
        $MSG_GIVESOURCE="轉移源碼";
        $MSG_IMPORT="導入";
        $MSG_EXPORT="導出";
        $MSG_UPDATE_DATABASE="更新資料庫";
        $MSG_ONLINE="在線";

?>
