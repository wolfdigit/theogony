<?php 
$cache_time=30; 
$OJ_CACHE_SHARE=true;
if (isset($_GET['cid'])) $ucid="&cid=".intval($_GET['cid']);
else $ucid="";
require_once("./include/db_info.inc.php");

	if(isset($OJ_LANG)){
		require_once("./lang/$OJ_LANG.php");
	}


?>

<?php $pr_flag=false;
$co_flag=false;
if (isset($_GET['id'])){
	// practice
	$id=intval($_GET['id']);
	require("oj-header.php");
	if (!isset($_SESSION['administrator']) && $id!=1000)
		$sql="SELECT * FROM `problem` WHERE `problem_id`=$id AND `defunct`='N' AND `problem_id` NOT IN (
				SELECT `problem_id` FROM `contest_problem` WHERE `contest_id` IN(
						SELECT `contest_id` FROM `contest` WHERE (`end_time`>NOW() or `private`='1') and `defunct`='N' ))
                                ";
	else
		$sql="SELECT * FROM `problem` WHERE `problem_id`=$id";

	$pr_flag=true;
}
else if (isset($_GET['cid']) && isset($_GET['pid'])){
	// contest
	$cid=intval($_GET['cid']);
	$pid=intval($_GET['pid']);

	if (!isset($_SESSION['administrator']))
		$sql="SELECT langmask, duration, end_time FROM `contest` WHERE `defunct`='N' AND `contest_id`=$cid AND `start_time`<NOW()";
	else
		$sql="SELECT langmask, duration, end_time FROM `contest` WHERE `defunct`='N' AND `contest_id`=$cid";
	$result=mysql_query($sql);
	$rows_cnt=mysql_num_rows($result);
   $ok_cnt=$rows_cnt==1;		
	$row=mysql_fetch_row($result);
	#$langmask=$row[0];
	$duration=$row[1] ;
	$endtime = strtotime($row[2]);
	mysql_free_result($result);
	if($duration!=0&&$endtime>time())
	{
		if(isset($_SESSION['user_id']))
		{
			$uid = $_SESSION['user_id'] ;
			$sql = "SELECT count(*) FROM `contest_user` WHERE `contest_id`=$cid AND `user_id`='$uid'" ;
			$result = mysql_query($sql) ;
			$row = mysql_fetch_row($result) ;
			mysql_free_result($result) ;
			if($row[0]==0) $ok_cnt = 0 ;
		}
		else
		{
			$ok_cnt = 0 ;
		}
	}
	if ($ok_cnt!=1){
		// not started
		echo "No such Contest!";
		require_once("oj-footer.php");
		exit(0);
	}else{
		// started
		$sql="SELECT * FROM `problem` WHERE `problem_id`=(
			SELECT `problem_id` FROM `contest_problem` WHERE `contest_id`=$cid AND `num`=$pid
			)";
	}
	// public
	require_once("contest-header.php");
	if (!$contest_ok){
		echo "Not Invited!";
		require_once("oj-footer.php");
		exit(1);
	}
	$co_flag=true;
}
else{
	require_once("oj-header.php");
	echo "<title>$MSG_NO_SUCH_PROBLEM</title><h2>$MSG_NO_SUCH_PROBLEM</h2>";
	require_once("oj-footer.php");
	exit(0);
}
$result=mysql_query($sql) or die(mysql_error());
if (mysql_num_rows($result)!=1){
   if(isset($_GET['id'])){
      $id=intval($_GET['id']);
	   mysql_free_result($result);
	   $sql="SELECT  contest.`contest_id` , contest.`title`,contest_problem.num FROM `contest_problem`,`contest` WHERE contest.contest_id=contest_problem.contest_id and `problem_id`=$id and defunct='N'  ORDER BY `num`";
	   //echo $sql;
           $result=mysql_query($sql);
	   if($i=mysql_num_rows($result)){
	      echo "This problem is in Contest(s) below:<br>";
		   for (;$i>0;$i--){
				$row=mysql_fetch_row($result);
				echo "<a href=problem.php?cid=$row[0]&pid=$row[2]>Contest $row[0]:$row[1]</a><br>";
				
			}
		}
		else{
			echo "<title>$MSG_NO_SUCH_PROBLEM!</title>";
			echo "<h2>$MSG_NO_SUCH_PROBLEM!</h2>";
		}
   }
	else{
		echo "<title>$MSG_NO_SUCH_PROBLEM!</title>";
		echo "<h2>$MSG_NO_SUCH_PROBLEM!</h2>";
	}
}
else{
	$row=mysql_fetch_object($result);
	echo "<center>\n";
	if ($pr_flag){
		echo "<title>$MSG_PROBLEM $row->problem_id. -- $row->title</title>";
		echo "<h2>$id: $row->title</h2>\n";
	}
	else{
		$PID="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		echo "<title>$MSG_PROBLEM $PID[$pid]: $row->title </title>";
		echo "<h2>$MSG_PROBLEM $PID[$pid]: $row->title</h2>\n";
	}
	echo "<span class=green>時間限制: </span>$row->time_limit Sec&nbsp;&nbsp;\n";
	echo "<span class=green>記憶體限制: </span>".$row->memory_limit." MB\n";
	if ($row->spj) echo "<span class=red>Special Judge</span>";
	
	echo "</center>";

	echo $row->description."<br/>";
	echo "<h3>輸入說明</h3>\n".$row->input."";
	echo "<h3>輸出說明</h3>\n".$row->output."";
	
	$ie6s="";
	$ie6e="";
	if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
	{
		$ie6s="<pre>";
		$ie6e="</pre>";
	}
	
	echo "<h3>範例輸入</h3>
			<div class=\"data\">".$ie6s.($row->sample_input).$ie6e."</div>";
	echo "<h3>範例輸出</h3>
			<div class=\"data\">".$ie6s.($row->sample_output).$ie6e."</div>";
	if ($pr_flag||true) 
		echo "<h3>提示</h3>
			<p>".nl2br($row->hint)."</p>";
	if ($pr_flag) 
		echo "<h3>來源</h3>
			<p><a href='problemset.php?search=$row->source'>".nl2br($row->source)."</a></p>";
	echo "<center>";
	if ($pr_flag){
		echo '<a href="submitpage.php?id='.$id.'" class="pure-button button-primary">' .$MSG_SUBMIT .'</a>&nbsp;&nbsp;';
	}else{
		echo '<a href="submitpage.php?cid='.$cid.'&pid='.$pid.'" class="pure-button button-primary">'.$MSG_SUBMIT.'</a>&nbsp;&nbsp;';
	}
	echo '<a href="problemstatus.php?id='.$row->problem_id.'" class="pure-button button-primary">'.$MSG_STATUS.'</a>&nbsp;&nbsp;';

	echo "</center>";
}
mysql_free_result($result);
?>
<?php require("oj-footer.php")?>
