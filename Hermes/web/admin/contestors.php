<?php require("admin-header.php");
require_once("../include/set_get_key.php");
if (!(isset($_SESSION['administrator']))){
	echo "<a href='../loginpage.php'>Please Login First!</a>";
	exit(1);
}

$ulist = "sysdavy, 910456, rocktest, rockwyc992, 910982, 910335, 110919, 110920super, 110920, startale, 911005, 010943, pscd, 011000, 010938, 011004, 010990, 010927, 010154, 110918, 010126, 110996";
$addPlist = "1028";

$clistSql = "SELECT `contest_id` FROM `contest` WHERE `ranked`='1' AND `defunct` = 'N'";
$uarr = explode(", ", $ulist);
$ulistQuo = array();
foreach ($uarr as $uid) {
	$ulistQuo[] = "'$uid'";
}
$ulistQuo = implode(", ", $ulistQuo);

$sql = "SELECT * FROM `contest_user` WHERE `user_id` IN ($ulistQuo)";
$result=mysql_query($sql) or die(mysql_error());
$userOpen = array();
foreach ($uarr as $uid) {
	$userOpen[$uid] = array();
}
while ($row=mysql_fetch_array($result)) {
	$userOpen[$row['user_id']][$row['contest_id']] = $row['start_time'];
}
mysql_free_result($result);

$finalSolSql = "SELECT MAX( solution_id ) FROM `solution` WHERE `contest_id` IN ( $clistSql ) AND `valid`='1' GROUP BY `user_id`, `problem_id`, `contest_id`";
$sql = "SELECT * FROM `solution` WHERE `solution_id` IN ( $finalSolSql ) AND `valid`='1' AND `user_id` IN ($ulistQuo) ORDER BY `user_id`, `contest_id`, `problem_id`";
$result=mysql_query($sql) or die(mysql_error());
$userContestSub = array();
foreach ($uarr as $uid) {
	$userContestSub[$uid] = array();
}
while ($row=mysql_fetch_array($result)) {
	$userContestSub[$row['user_id']][$row['problem_id']] = array('result'=>$row['result'], 'contest_id'=>$row['contest_id'], 'in_date'=>$row['in_date']);
}
mysql_free_result($result);

//$plistSql = "( SELECT `problem_id` FROM `contest_problem` WHERE `contest_id` IN ( $clistSql ) ) UNION ( SELECT `problem_id` FROM `problem` WHERE `problem_id` IN ( $addPlist ) )";
$conPlistSql = "SELECT `problem_id` FROM `contest_problem` WHERE `contest_id` IN ( $clistSql )";
$sql = "SELECT DISTINCT `user_id`, `problem_id`, `result` FROM `solution` WHERE (`problem_id` IN ( $conPlistSql ) OR `problem_id` IN ($addPlist)) AND `user_id` IN ($ulistQuo) ORDER BY `user_id`, `problem_id`";
//$sql = $plistSql;
$result=mysql_query($sql) or die(mysql_error());
$userPracSub = array();
foreach ($uarr as $uid) {
	$userPracSub[$uid] = array();
}
while ($row=mysql_fetch_array($result)) {
	//print_r($row);
	if ($userPracSub[$row['user_id']][$row['problem_id']]!=4) {
		$userPracSub[$row['user_id']][$row['problem_id']] = $row['result'];
	}
}
mysql_free_result($result);
//print_r($userPracSub);

echo "<table border=1>";
echo "<tr><td>id</td><td>open</td><td>submit/AC(contest-problem)</td><td>sub/AC(contest+practice)</td><td>score</td></tr>";
foreach ($userOpen as $uid=>$val1) {
	$score = 0;
	echo "<tr>";
	echo "<td>$uid</td>";
	echo "<td>";
	foreach ($val1 as $cid=>$time) {
		echo "$cid <small>($time)</small><br />";
	}
	echo "</td>";

	echo "<td>";
	foreach ($userContestSub[$uid] as $pid=>$val2) {
		if ($val2['result']==4) {
			echo "V ";
			$score += (1-0.4);
		}
		echo $val2['contest_id']."-".$pid."<small>(".$val2['in_date'].")</small><br />";
	}
	echo "</td>";

	echo "<td>";
	foreach ($userPracSub[$uid] as $pid=>$result) {
		if ($result==4) {
			echo "V ";
			$score += 0.4;
		}
		echo "$pid<br />";
	}
	echo "</td>";
	echo "<td>$score</td>";

	echo "</tr>";
}
echo "</table>";
?>
