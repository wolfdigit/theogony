<?php require("admin-header.php");
require_once("../include/set_get_key.php");
require_once("../include/user.inc.php");
if (!(isset($_SESSION['administrator']))){
	echo "<a href='../loginpage.php'>Please Login First!</a>";
	exit(1);
}
?>
<title>User List</title>
<center><h2>User List</h2></center>
<center>
<table width=90%>
<tr class=toprow><td>ID</td><td>nick</td><td>email</td><td>school</td>
<td>Admin<br/>SourceBrowser<br/>ContestCreator<br/>HttpJudge<br/>Invisible</td>
<td>defunct</td></tr>
<?php
$UList = User::getUserList();
foreach ($UList as $cnt=>$U) {
	if ($cnt&1) echo "<tr class=oddrow>";
	else        echo "<tr class=evenrow>";

	$privLong = array('administrator', 'source_browser', 'contest_creator', 'http_judge', 'invisible');
	$privShort = array('A', 'S', 'C', 'H', 'I');
	$color = array();
	$showPrivStr = "";
	foreach ($privLong as $i=>$long) {
		if (in_array($long, $U->privilege)) {
			#$showPrivStr .= " <a href='user_priv.php?getkey=".$_SESSION['getkey']."&uid=$U->user_id&priv=$long&del'><font color=red>".$privShort[$i]."</font></a>";
			$showPrivStr .= " <font color=red>".$privShort[$i]."</font>";
		}
		else {
			#$showPrivStr .= " <a href='user_priv.php?getkey=".$_SESSION['getkey']."&uid=$U->user_id&priv=$long&set'><font color=gray>".$privShort[$i]."</font></a>";
			$showPrivStr .= " <font color=gray>".$privShort[$i]."</font>";
		}
	}

	if ($U->defunct=='N') $defunctStr = $U->defunct;
	else                  $defunctStr = "<font color=red>$U->defunct</font>";
	
?>
	<td><a href="user_edit.php?user=<?=$U->user_id?>"><?=$U->user_id?></a></td>
	<td><a href="../userinfo.php?user=<?=$U->user_id?>"><?=$U->nick?></a></td>
	<td><?=$U->email?></td>
	<td><?=$U->school?></td>
	<td><?=$showPrivStr?></td>
	<td><?=$defunctStr?></td>
<?php
	echo "</tr>";
}
?>
</table>
</center>
<?php
require("../oj-footer.php");
?>
