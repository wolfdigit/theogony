<?php require_once("./include/db_info.inc.php");
$OJ_CACHE_SHARE=true;
	$cache_time=30;
if(isset($OJ_LANG)){
		require_once("./lang/$OJ_LANG.php");
}
require_once("./include/const.inc.php");
require_once("./include/my_func.inc.php");
class TM{
	var $solved=0;
	var $time=0;
	var $p_wa_num;
	var $p_ac_sec;
	var $user_id;
        var $nick;
	var $dur ;
	function TM($duration){
		$this->solved=0;
		$this->time=0;
		$this->p_wa_num=array();
		$this->p_ac_sec=array();
		$this->dur = $duration ;
	}
	function Add($pid,$sec,$res,$penalty){
//		echo "Add $pid $sec $res<br>";
		if (isset($this->p_ac_sec[$pid])&&$this->dur==0)
			return;
		if ($res!=4){
			if(isset($this->p_wa_num[$pid])&&$this->dur==0){
				$this->p_wa_num[$pid]++;
			}else{
				$this->p_wa_num[$pid]=1;
			}
			if(isset($this->p_ac_sec[$pid])&&$this->dur!=0)
			{
				$this->time -= $this->p_ac_sec[$pid] ;
				unset($this->p_ac_sec[$pid]) ;
				$this->solved-- ;
			}
		}else{
			if(!isset($this->p_ac_sec[$pid])) $this->solved++;
			else $this->time -= $this->p_ac_sec[$pid] ;
			$this->p_ac_sec[$pid]=$sec;
			if(!isset($this->p_wa_num[$pid])||$this->dur!=0) $this->p_wa_num[$pid]=0;
			$this->time+=$sec+$this->p_wa_num[$pid]*$penalty;
//			echo "Time:".$this->time."<br>";
//			echo "Solved:".$this->solved."<br>";
		}
	}
}

function s_cmp($A,$B){
//	echo "Cmp....<br>";
	if ($A->solved!=$B->solved) return $A->solved<$B->solved;
	else return $A->time>$B->time;
}

// contest start time
if (!isset($_GET['cid'])) die("No Such Contest!");
$cid=intval($_GET['cid']);
require_once("contest-header.php");
$sql="SELECT `start_time`,`title`,`duration`,`penalty`,`end_time` FROM `contest` WHERE `contest_id`='$cid'";
$result=mysql_query($sql) or die(mysql_error());
$rows_cnt=mysql_num_rows($result);
$start_time=0;
$duration=0;
$penalty=20 ;
$end_time = 0 ; 
if ($rows_cnt>0){
	$row=mysql_fetch_array($result);
	$start_time=strtotime($row[0]);
	$title=$row[1];
	$duration=$row[2] ;
	$penalty = $row[3] ;
	$end_time = strtotime($row[4]) ;
}
mysql_free_result($result);
if ($start_time==0){
	echo "No Such Contest";
	require_once("oj-footer.php");
	exit(0);
}

if (!$contest_ok){
	echo "<br><h1>Not Invited!</h1>";
	require_once("oj-footer.php");
	exit(1);
}

if ($start_time>time()){
	echo "Contest Not Started!";
	require_once("oj-footer.php");
	exit(0);
}

if(!isset($_SESSION['administrator'])&&$end_time>=time()&&$duration!=0) {
	echo "Rank will be available after contest ends" ;
	require_once("oj-footer.php") ;
	exit(0) ;
}

$sql="SELECT count(1) FROM `contest_problem` WHERE `contest_id`='$cid'";
$result=mysql_query($sql);
$row=mysql_fetch_array($result);
$pid_cnt=intval($row[0]);
mysql_free_result($result);

if($duration==0)
{
$sql="SELECT 
	users.user_id,users.nick,solution.result,solution.num,solution.in_date 
		FROM 
			(select * from solution where solution.contest_id='$cid' and num>=0) solution 
		left join users 
		on users.user_id=solution.user_id 
	ORDER BY users.user_id,in_date";
}
else
{
$wherestr = "users.user_id NOT IN (SELECT user_id FROM privilege WHERE (rightstr='administrator' OR rightstr='invisible') AND defunct='N')" ;
if(isset($_SESSION['administrator'])) $wherestr = "1" ;
$sql="SELECT users.user_id, users.nick, solution.result, solution.num, solution.in_date, contest_user.start_time
FROM (
 SELECT *
 FROM solution
 WHERE solution.contest_id = '$cid'
 AND num >=0
)solution
LEFT JOIN users ON users.user_id = solution.user_id
LEFT JOIN contest_user ON contest_user.user_id = solution.user_id
AND contest_user.contest_id = solution.contest_id
WHERE $wherestr
ORDER BY users.user_id, in_date" ;
}
//echo $sql;
$result=mysql_query($sql);
$user_cnt=0;
$user_name='';
$U=array();
$user_start_time ;
while ($row=mysql_fetch_object($result)){
	$n_user=$row->user_id;
	if (strcmp($user_name,$n_user)){
		$user_cnt++;
		$U[$user_cnt]=new TM($duration);
		$U[$user_cnt]->user_id=$row->user_id;
                $U[$user_cnt]->nick=$row->nick;

		$user_name=$n_user;
		if($duration!=0)
		{
			$user_start_time = strtotime($row->start_time) ;
		}
	}
	if($duration==0) $U[$user_cnt]->Add($row->num,strtotime($row->in_date)-$start_time,intval($row->result),$penalty*60);
	else $U[$user_cnt]->Add($row->num,strtotime($row->in_date)-$user_start_time,intval($row->result),0);
}
mysql_free_result($result);
usort($U,"s_cmp");
$rank=1;
echo "<style> td{font-size:14} </style>";
echo "<title>Contest RankList -- $title</title>";
echo "<center><h3>Contest RankList -- $title</h3><a href=contestrank.xls.php?cid=$cid>Download</a></center>";
echo '<table class="pure-table"><thead><tr align=center><td width=5%>Rank<td width=10%>User<td width=10%>Nick<td width=5%>Solved<td width=5%>Penalty';

//Listing Problems ID
for ($i=0;$i<$pid_cnt;$i++)
	echo "<td><a href=problem.php?cid=$cid&pid=$i>$PID[$i]</a>";
echo "</tr></thead><tbody>";

//Listing User Status
for ($i=0;$i<$user_cnt;$i++){
	if ($i&1) echo '<tr class="pure-table-odd" align=center>';
	else echo "<tr align=center>";
	echo "<td>$rank";
	$rank++;
	$uuid=$U[$i]->user_id;

	$usolved=$U[$i]->solved;
	echo "<td><a href=userinfo.php?user=$uuid>$uuid</a>";
	echo "<td><a href=userinfo.php?user=$uuid>".$U[$i]->nick."</a>";
	echo "<td><a href=status.php?user_id=$uuid&cid=$cid>$usolved</a>";
	echo "<td>".sec2str($U[$i]->time);
	for ($j=0;$j<$pid_cnt;$j++){
		$color="72962e";
		if (isset($U[$i]->p_ac_sec[$j])&&$U[$i]->p_ac_sec[$j]>0){
			$bg_color="72962e";
		}else if(isset($U[$i]->p_wa_num[$j])&&$U[$i]->p_wa_num[$j]>0) {
			$bg_color="FF0000";
		}
		
		
		echo "<td style=\"color:#$bg_color;\">";
		if(isset($U[$i])){
			if (isset($U[$i]->p_ac_sec[$j])&&$U[$i]->p_ac_sec[$j]>0)
				echo sec2str($U[$i]->p_ac_sec[$j]);
			if (isset($U[$i]->p_wa_num[$j])&&$U[$i]->p_wa_num[$j]>0) 
				echo "(-".$U[$i]->p_wa_num[$j].")";
		}
	}
	echo "</tr>";
}
echo "</tbody></table>";

?>
<?php require_once("oj-footer.php")?>

