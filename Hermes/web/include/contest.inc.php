<?php
$PID="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

class Contest {
	public $cid;
	public $title;
	public $start_time;
	public $end_time;
	public $private;
	public $penalty;
	public $duration;
	public $ranked;
	public $user;
	private $_r, $_e, $_s;

	static function getContest($cid) {
		$sql="SELECT * FROM `contest` WHERE `contest_id`='$cid' AND `defunct`='N' ";
		$result=mysql_query($sql);
		$rows_cnt=mysql_num_rows($result);
		mysql_free_result($result);
		if ($rows_cnt==0){
			return NULL;
		}
		else {
			return new Contest($cid);
		}
	}

	static function getContestList() {
		$CList = array();
		$sql="SELECT * FROM `contest` WHERE `defunct`='N' ORDER BY `contest_id` DESC";
		$result=mysql_query($sql);
		while ($row=mysql_fetch_object($result)){
			$CList[] = new Contest($row->contest_id,$row->title,$row->start_time,$row->end_time,$row->duration,$row->private,$row->ranked);
		}
		mysql_free_result($result);

		return $CList;
	}

	private function __construct($cid) {
		$this->_r = $this->_e = $this->_s = -1;
		if (func_num_args()==1) {
			$this->cid = $cid;
			$sql="SELECT * FROM `contest` WHERE `contest_id`='$cid' AND `defunct`='N' ";
			$result=mysql_query($sql);
			$row=mysql_fetch_object($result);
			mysql_free_result($result);
			$this->title = $row->title;
			$this->start_time = $row->start_time;
			$this->end_time = $row->end_time;
			$this->private = $row->private;
			$this->penalty = $row->penalty;
			$this->duration = $row->duration;
			$this->ranked = $row->ranked;
			$this->user = NULL;
		}
		else {
			list($cid,$title,$start_time,$end_time,$duration,$private,$ranked) = func_get_args();
			$this->cid = $cid;
			$this->title = $title;
			$this->start_time = $start_time;
			$this->end_time = $end_time;
			$this->private = $private;
			$this->duration = $duration;
			$this->ranked = $ranked;
			$this->user = NULL;
		}
	}

	function setUser($uid) {
		$this->user = new ContestUser($this, $uid);
	}

	function running() {
		#return (!$this->ended()&&!$this->notstart());
		if ($this->_r==-1) $this->_r = (!$this->ended()&&!$this->notstart());
		return $this->_r;
	}
	function ended() {
		#return (time()>strtotime($this->end_time));
		if ($this->_e==-1) $this->_e = (time()>strtotime($this->end_time));
		return $this->_e;
	}
	function notstart() {
		#return (time()<strtotime($this->start_time));
		if ($this->_s==-1) $this->_s = (time()<strtotime($this->start_time));
		return $this->_s;
	}

	function getProblemList() {
		$probList = array();

		$sql="SELECT `problem`.`title` as `title`,`problem`.`problem_id` as `pid`
			FROM `contest_problem`,`problem`
			WHERE `contest_problem`.`problem_id`=`problem`.`problem_id`
			AND `contest_problem`.`contest_id`='$this->cid' ORDER BY `contest_problem`.`num`";
		$result=mysql_query($sql);
		$cnt=0;
		while ($row=mysql_fetch_object($result)){
			$probList[] = new ContestProb($cnt, $row->pid, $row->title, $this);
			$cnt++;
		}
		mysql_free_result($result);

		return $probList;
	}
}

class ContestUser {
	public $Contest;
	public $uid;
	public $user_start;

	function __construct($Contest, $uid) {
		$this->Contest = $Contest;
		$this->uid = $uid;
		$sql = "SELECT `start_time` FROM `contest_user` WHERE `contest_id`='$Contest->cid' AND `user_id`='$uid'" ;
		$result = mysql_query($sql) ;
		$row=mysql_fetch_object($result);
		mysql_free_result($result);
		if ($row) {
			$this->user_start = $row->start_time;
		}
		else {
			$this->user_start = FALSE;
		}
	}

	function startContest() {
		$Contest = $this->Contest;
		$cur_time = date("Y-m-d H:i:s") ;
		$sql = "INSERT INTO `contest_user`(`contest_id`,`user_id`,`start_time`) VALUES ('$Contest->cid','$this->uid','$cur_time')" ;
		mysql_query($sql) or die(mysql_error()) ;
		$this->user_start = $cur_time;
	}

	function user_end_time() {
		if ($this->user_start) return strtotime($this->user_start)+$this->Contest->duration*60;
		else return FALSE;
	}
}

class ContestProb {
	public $Contest;
	public $cpid;
	public $pid;
	public $title;

	function __construct($cpid, $pid, $title, $Contest) {
		$this->cpid = $cpid;
		$this->pid=  $pid;
		$this->title = $title;
		$this->Contest = $Contest;
	}

	function check_ac() {
		if (func_num_args()==0) {
			if (isset($_SESSION['user_id'])) {
				$uid = $_SESSION['user_id'];
			}
			else {
				return FALSE;
			}
		}
		else {
			list($uid) = func_get_args();
		}
		$sql="SELECT count(*) FROM `solution` WHERE `contest_id`='".$this->Contest->cid."' AND `problem_id`='$this->pid' AND `result`='4' AND `user_id`='$uid'";
		$result=mysql_query($sql);
		$row=mysql_fetch_array($result);
		mysql_free_result($result);
		$ac=intval($row[0]);
		if ($ac>0) return TRUE;
		else       return FALSE;
	}

	function check_try() {
		if (func_num_args()==0) {
			if (isset($_SESSION['user_id'])) {
				$uid = $_SESSION['user_id'];
			}
			else {
				return FALSE;
			}
		}
		else {
			list($uid) = func_get_args();
		}
		$sql="SELECT count(*) FROM `solution` WHERE `contest_id`='".$this->Contest->cid."' AND `problem_id`='$this->pid' AND `user_id`='$uid'";
		$result=mysql_query($sql);
		$row=mysql_fetch_array($result);
		mysql_free_result($result);
		$sub=intval($row[0]);
		if ($sub>0) return TRUE;
		else        return FALSE;
	}
}
?>
