<?php
	$OJ_CACHE_SHARE=false;
	$cache_time=30;
  require_once ("oj-header.php");
  require_once ("./include/db_info.inc.php");
?>
<title>Rank List</title>
<?php
	if(isset($OJ_LANG)){
		require_once("./lang/$OJ_LANG.php");
	}
	$adminstr = "" ;
	if(!isset($_SESSION['administrator'])) $adminstr = "AND end_time < NOW()" ;
	$start = 0;
	if (isset($_GET['start']))
		$start = intval($_GET['start']);
	$page_size=50;
/*	$sql = "
SELECT D.user_id, E.nick, D.solved, D.penalty
FROM (

  SELECT user_id, count( * ) AS solved, sum( C.penalty ) AS penalty
  FROM (

    SELECT B.user_id, B.contest_id, B.problem_id, TIME_TO_SEC( TIMEDIFF(
    B.in_date, A.start_time ) ) AS penalty
    FROM (

      SELECT user_id, contest_id, start_time
      FROM contest_user
    ) AS A, (

      SELECT user_id, contest_id, problem_id, in_date
      FROM solution
      WHERE solution_id
      IN (

        SELECT MAX( solution_id )
        FROM solution
        WHERE contest_id
        IN (

          SELECT contest_id
          FROM contest
          WHERE ranked = '1'
          AND defunct = 'N'
          $adminstr
        )
      AND valid = '1'
      GROUP BY user_id, problem_id, contest_id
      )
      AND result = '4'
      AND valid = '1'
    ) AS B
    WHERE A.user_id = B.user_id
    AND A.contest_id = B.contest_id
  ) AS C
  GROUP BY user_id
  ORDER BY solved DESC , penalty
) AS D, `users` AS E
WHERE D.user_id = E.user_id
";*/	
	$sql="
SELECT solution.user_id, users.nick, COUNT( DISTINCT solution.problem_id ) AS solved, SUM( TIME_TO_SEC( TIMEDIFF( solution.in_date, contest_user.start_time ) ) ) AS penalty
FROM solution
INNER JOIN contest_user ON solution.user_id = contest_user.user_id
AND solution.contest_id = contest_user.contest_id
INNER JOIN users ON solution.user_id = users.user_id
WHERE solution.solution_id
IN (
  SELECT MAX( solution_id )
  FROM solution
  INNER JOIN contest ON solution.contest_id = contest.contest_id
  WHERE contest.ranked = '1'
  AND contest.defunct = 'N' 
  $adminstr
  AND solution.valid = '1'
  GROUP BY user_id, problem_id, solution.contest_id
)
AND solution.result = '4'
AND solution.valid = '1'
GROUP BY solution.user_id, users.nick
ORDER BY solved DESC , penalty ASC
";
		$result = mysql_query ( $sql ); //mysql_error();
?>
<center><table width=60%>
<tr class='toprow'>
<td width=10% align=center><b><?=$MSG_Number?></b></td>
<td width=20% align=center><b><?=$MSG_USER?></b></td>
<td width=30% align=center><b><?=$MSG_NICK?></b></td>
<td width=20% align=center><b><?=$MSG_AC?></b></td>
<td width=20% align=center><b>penalty</b></td</tr>
<? for ($rank = 1; $row = mysql_fetch_object($result); $rank++): ?>
<tr class='<?=$rank % 2 == 1 ? "oddrow" : "evenrow"?>'>
<td align=center><?=$rank?></td>
<td align=center><a href='userinfo.php?user=<?=$row->user_id?>'><?=$row->user_id?></a></td>
<td align=center><?=htmlspecialchars ( $row->nick )?></td>
<td align=center><a href='status.php?user_id=<?=$row->user_id?>&jresult=4'><?=$row->solved?></a>
<td align=center><a href='status.php?user_id=<?=$row->user_id?>'><?=$row->penalty?></a>
</tr>
<? endfor; # ($rank = 0; $row = mysql_fetch_object($result); $rank++) ?>
</table>
<?php require_once ("oj-footer.php");?>
