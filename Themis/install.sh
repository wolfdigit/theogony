#!/bin/sh
FILEHOST=127.0.0.1

if [ "x$FILEHOST" = "x0.0.0.0" ]; then
	echo "plz edit Themis/install.sh to set FILEHOST properly"
	return 1
fi

# essential utils
sudo apt-get install flex g++ libmysql++-dev mono-gmcs make
sudo yum -y install gcc-c++  mysql-devel glibc-static make

#work dir
sudo useradd -m -u 1536 judge
sudo cp -r judge_home/* /home/judge/
sudo chown -R judge /home/judge
sudo chmod 700 /home/judge/data /home/judge/etc /home/judge/run?

#secured file transfer
sudo ssh-keygen
sudo cat /root/.ssh/id_rsa.pub
echo "copy this key to /home/judge/.ssh/authorized_keys @ $FILEHOST"
echo "(cmd: ""sudo -u judge vim /home/judge/.ssh/authorized_keys"" on $FILEHOST)"
read DUMMY
echo "this time it may ask to confirm host key, but no passwd:"
read DUMMY
sudo ssh judge@$FILEHOST echo "SUCCESS: it should not ask for passwd"

#compile & install daemon
./make.sh
sudo ./make.sh install
./make.sh clean

echo "edit ""/home/judge/etc/judge.conf"" configuration:"
read DUMMY
sudo vim /home/judge/etc/judge.conf

#start daemon
sudo cp init.d/judged /etc/init.d/judged
sudo chmod +x /etc/init.d/judged
sudo ln -s /etc/init.d/judged /etc/rc3.d/S93judged
sudo ln -s /etc/init.d/judged /etc/rc2.d/S93judged
sudo /etc/init.d/judged start
