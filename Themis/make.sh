case "$1" in
  clean)
	cd judged
	make clean
	cd ../judge_client
	make clean
	cd ../sim/sim_2_26
	make clean
	;;
  install)
	killall /usr/bin/judged

	cp judged/judged /usr/bin
	cp judge_client/judge_client /usr/bin
	cp sim/sim_2_26/sim_c /usr/bin
	cp sim/sim_2_26/sim_java /usr/bin/sim_java
	cp sim/sim_2_26/sim_pasc /usr/bin/sim_pas
	cp sim/sim.sh /usr/bin
	ln -s /usr/bin/sim_c /usr/bin/sim_cc
	ln -s /usr/bin/sim_c /usr/bin/sim_rb
	ln -s /usr/bin/sim_c /usr/bin/sim_sh
	;;
  *)
	cd judged
	make
	cd ../judge_client
	make
	cd ../sim/sim_2_26
	make sim_c
	make sim_java
	make sim_pasc
	;;
esac
